import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:vacancy/core/usecases/usecase.dart';
import 'package:vacancy/domain/repositories/person_repository.dart';

import '../../core/error/failure.dart';
import '../entities/heroes_entity.dart';

class GetAllPerson extends UseCase<List<HeroesEntity>, PageHeroesParams> {
  final PersonRepository personRepository;
  GetAllPerson(this.personRepository);

  Future<Either<Failure, List<HeroesEntity>>> call(
      PageHeroesParams params) async {
    return await personRepository.getAllPerson(params.page);
  }
}

class PageHeroesParams extends Equatable {
  final int page;

  PageHeroesParams({required this.page});

  @override
  List<Object?> get props => [page];
}
