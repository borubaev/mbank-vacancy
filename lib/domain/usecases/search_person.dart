import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:vacancy/core/usecases/usecase.dart';
import 'package:vacancy/domain/repositories/person_repository.dart';

import '../../core/error/failure.dart';
import '../entities/heroes_entity.dart';

class SearchPerson extends UseCase<List<HeroesEntity>, SearchPersonParams> {
  final PersonRepository personRepository;
  SearchPerson(this.personRepository);

  Future<Either<Failure, List<HeroesEntity>>> call(
      SearchPersonParams params) async {
    return await personRepository.searchPerson(params.name);
  }
}

class SearchPersonParams extends Equatable {
  final String name;

  SearchPersonParams({required this.name});

  @override
  List<Object?> get props => [name];
}
