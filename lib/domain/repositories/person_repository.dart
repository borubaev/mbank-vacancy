import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:vacancy/domain/entities/heroes_entity.dart';

import '../../core/error/failure.dart';

abstract class PersonRepository {
  Future<Either<Failure, List<HeroesEntity>>> getAllPerson(int page);
  Future<Either<Failure, List<HeroesEntity>>> searchPerson(String name);
}
