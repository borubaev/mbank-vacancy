import 'package:equatable/equatable.dart';

import 'location_entity.dart';

class HeroesEntity extends Equatable {
  final int id;
  final String name;
  final String status;
  final String species;
  final String type;
  final String gender;
  final EntityLocation origin;
  final EntityLocation location;
  final String img;
  final DateTime created;
  final List<String> episode;

  const HeroesEntity({
    required this.id,
    required this.name,
    required this.status,
    required this.species,
    required this.type,
    required this.gender,
    required this.origin,
    required this.location,
    required this.img,
    required this.created,
    required this.episode,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [
        id,
        name,
        status,
        species,
        type,
        gender,
        origin,
        location,
        img,
        created,
        episode,
      ];
}
