class EntityLocation {
  final String name;
  final String url;

  const EntityLocation({required this.name, required this.url});
}
