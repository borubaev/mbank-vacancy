import 'package:flutter/material.dart';
import 'package:vacancy/presentation/pages/episode_screen.dart';

import '../widgets/custom_search_delegate.dart';
import '../widgets/heroes_list_widget.dart';

class HomePage extends StatefulWidget {
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedIndex = 0;
  bool isSelected = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('${isSelected ? 'Rick and Morty' : 'Episodes'}'),
        centerTitle: true,
        leading: isSelected ? Icon(Icons.format_list_bulleted_sharp) : null,
        actions: [
          IconButton(
            icon: Icon(Icons.search),
            color: Colors.white,
            onPressed: () {
              showSearch(context: context, delegate: CustomSearchDelegate());
            },
          )
        ],
      ),
      body: isSelected ? PersonsList() : EpisodePage(),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: IconButton(
              icon: Icon(Icons.person_outline_outlined),
              onPressed: () {
                setState(() {
                  isSelected = true;
                  _selectedIndex = 0;
                });
              },
            ),
            label: 'Heroes',
          ),
          BottomNavigationBarItem(
            icon: IconButton(
              onPressed: () {
                setState(() {
                  isSelected = false;
                  _selectedIndex = 1;
                });
              },
              icon: Icon(Icons.video_collection_outlined),
            ),
            label: 'Episodes',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],
        // onTap: (int index) {
        //   setState(() {
        //     _selectedIndex = index;
        //     print(index);
        //   });
        // },
      ),
    );
  }
}
