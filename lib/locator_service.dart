import 'package:get_it/get_it.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vacancy/presentation/bloc/heroes_list_cubit/heroes_list_cubit.dart';
import 'package:vacancy/presentation/bloc/search_bloc/search_bloc.dart';
import 'core/platform/network_info.dart';
import 'data/datasource/heroes_local_datasource.dart';
import 'data/datasource/heroes_remote_datasource.dart';
import 'data/repositories/heroes_repository.dart';
import 'domain/repositories/person_repository.dart';
import 'domain/usecases/get_all_persons.dart';
import 'domain/usecases/search_person.dart';

import 'package:http/http.dart' as http;

final sl = GetIt.instance;

Future<void> init() async {
  // BLoC / Cubit
  sl.registerFactory(
    () => PersonListCubit(getAllPersons: sl()),
  );
  sl.registerFactory(
    () => PersonSearchBloc(searchPerson: sl()),
  );

  // UseCases
  sl.registerLazySingleton(() => GetAllPerson(sl()));
  sl.registerLazySingleton(() => SearchPerson(sl()));

  // Repository
  sl.registerLazySingleton<PersonRepository>(
    () => HeroesRepositoryImpl(
      remoteDataSource: sl(),
      localDataSource: sl(),
      networkInfo: sl(),
    ),
  );

  sl.registerLazySingleton<HeroesRemoteDatasource>(
    () => HeroesRemoteDataSourceImpl(
      client: sl(),
    ),
  );

  sl.registerLazySingleton<HeroesLocalDataSource>(
    () => HeroesLocalDataSourceImpl(sharedPreferences: sl()),
  );

  // Core
  sl.registerLazySingleton<NetworkInfo>(
    () => NetworkInfoImp(sl()),
  );

  // External
  final sharedPreferences = await SharedPreferences.getInstance();
  sl.registerLazySingleton(() => sharedPreferences);
  sl.registerLazySingleton(() => http.Client());
  sl.registerLazySingleton(() => InternetConnectionChecker());
}
