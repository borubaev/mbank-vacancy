import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:vacancy/core/error/exception.dart';
import 'package:vacancy/data/model/heroes_model.dart';

abstract class HeroesLocalDataSource {
  Future<List<HeroesModel>> getLastHeroesFromCache();
  Future<void> heroesToCache(List<HeroesModel> heroes);
}

const CACHED_PERSONS_LIST = 'CACHED_PERSONS_LIST';

class HeroesLocalDataSourceImpl implements HeroesLocalDataSource {
  final SharedPreferences sharedPreferences;

  HeroesLocalDataSourceImpl({required this.sharedPreferences});

  @override
  Future<List<HeroesModel>> getLastHeroesFromCache() {
    final jsonHeroesList = sharedPreferences.getStringList(CACHED_PERSONS_LIST);
    if (jsonHeroesList!.isNotEmpty) {
      return Future.value(jsonHeroesList
          .map((e) => HeroesModel.fromJson(jsonDecode(e)))
          .toList());
    } else {
      throw CacheException();
    }
  }

  @override
  Future<void> heroesToCache(List<HeroesModel> heroes) {
    final List<String> jsonHeroesList =
        heroes.map((character) => jsonEncode(character.toJson())).toList();

    sharedPreferences.setStringList(CACHED_PERSONS_LIST, jsonHeroesList);
    return Future.value(jsonHeroesList);
  }
}
