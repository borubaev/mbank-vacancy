import 'dart:convert';

import 'package:vacancy/core/error/exception.dart';
import 'package:vacancy/data/model/heroes_model.dart';
import 'package:http/http.dart' as http;

abstract class HeroesRemoteDatasource {
  Future<List<HeroesModel>> getAllPerson(int page);

  Future<List<HeroesModel>> searchPerson(String name);
}

class HeroesRemoteDataSourceImpl implements HeroesRemoteDatasource {
  final http.Client client;
  HeroesRemoteDataSourceImpl({required this.client});

  @override
  Future<List<HeroesModel>> getAllPerson(int page) =>
      getHeroesFromUrl('https://rickandmortyapi.com/api/character/?page=$page');

  @override
  Future<List<HeroesModel>> searchPerson(String name) =>
      getHeroesFromUrl('https://rickandmortyapi.com/api/character/?name=$name');

  Future<List<HeroesModel>> getHeroesFromUrl(String url) async {
    final response = await client
        .get(Uri.parse(url), headers: {'Content-Type': 'application/json'});

    if (response.statusCode == 200) {
      final heroes = json.decode(response.body);
      return (heroes['results'] as List)
          .map((character) => HeroesModel.fromJson(character))
          .toList();
    } else {
      throw ServerException();
    }
  }
}
