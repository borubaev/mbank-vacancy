import 'package:vacancy/data/model/location_model.dart';
import 'package:vacancy/domain/entities/heroes_entity.dart';

class HeroesModel extends HeroesEntity {
  HeroesModel({
    required id,
    required name,
    required status,
    required species,
    required type,
    required gender,
    required origin,
    required location,
    required img,
    required episode,
    required created,
  }) : super(
          id: id,
          name: name,
          status: status,
          species: species,
          type: type,
          gender: gender,
          origin: origin,
          location: location,
          img: img,
          episode: episode,
          created: created,
        );

  factory HeroesModel.fromJson(Map<String, dynamic> json) {
    return HeroesModel(
        id: json['id'],
        name: json['name'],
        status: json['status'],
        species: json['species'],
        type: json['type'],
        gender: json['gender'],
        origin: json['origin'] != null
            ? LocationModel.fromJson(json['origin'])
            : null,
        location: json['location'] != null
            ? LocationModel.fromJson(json['origin'])
            : null,
        img: json['image'],
        episode:
            (json['episode'] as List<dynamic>).map((e) => e as String).toList(),
        created: DateTime.parse(json['created'] as String));
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'status': status,
      'species': species,
      'type': type,
      'gender': gender,
      'origin': origin,
      'location': location,
      'image': img,
      'episode': episode,
      'created': created.toIso8601String()
    };
  }
}
