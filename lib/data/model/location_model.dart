import 'package:vacancy/domain/entities/location_entity.dart';

class LocationModel extends EntityLocation {
  LocationModel({name, url}) : super(name: name, url: url);
  factory LocationModel.fromJson(Map<String, dynamic> json) {
    return LocationModel(
      name: json['name'],
      url: json['url'],
    );
  }

  Map<String, dynamic> toJson() {
    return {'name': name, 'url': url};
  }
}
