import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';
import 'package:vacancy/domain/entities/heroes_entity.dart';

import '../../core/error/exception.dart';
import '../../core/error/failure.dart';
import '../../core/platform/network_info.dart';
import '../../domain/repositories/person_repository.dart';
import '../datasource/heroes_local_datasource.dart';
import '../datasource/heroes_remote_datasource.dart';
import '../model/heroes_model.dart';

class HeroesRepositoryImpl implements PersonRepository {
  final HeroesRemoteDatasource remoteDataSource;
  final HeroesLocalDataSource localDataSource;
  final NetworkInfo networkInfo;

  HeroesRepositoryImpl({
    required this.remoteDataSource,
    required this.localDataSource,
    required this.networkInfo,
  });

  @override
  Future<Either<Failure, List<HeroesEntity>>> getAllPerson(int page) async {
    return await _getPersons(() {
      return remoteDataSource.getAllPerson(page);
    });
  }

  @override
  Future<Either<Failure, List<HeroesEntity>>> searchPerson(String name) async {
    return await _getPersons(() {
      return remoteDataSource.searchPerson(name);
    });
  }

  Future<Either<Failure, List<HeroesModel>>> _getPersons(
      Future<List<HeroesModel>> Function() getPersons) async {
    if (await networkInfo.isConnected) {
      try {
        final remotePerson = await getPersons();
        localDataSource.heroesToCache(remotePerson);
        return Right(remotePerson);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      try {
        final localPerson = await localDataSource.getLastHeroesFromCache();
        return Right(localPerson);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }
}
